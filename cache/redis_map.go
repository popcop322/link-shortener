package cache

import "fmt"

type CacheMap struct {
	m map[string]string
}

func (r CacheMap) SetString(req string, id string) error {
	r.m[id] = req
	fmt.Println(r.m)
	return nil
}

func (r CacheMap) GetString(shortUrl string) (string, error) {
	return r.m[shortUrl], nil
}

func NewRedisMap() CacheMap {
	var r CacheMap
	r.m = make(map[string]string)
	return r
}
