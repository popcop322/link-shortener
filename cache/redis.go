package cache

import (
	"github.com/gomodule/redigo/redis"
)

type Cache interface {
	SetString(req string, id string) error
	GetString(shortUrl string) (string, error)
}

type Redis struct {
	connection redis.Conn
}

func (r Redis) SetString(req string, id string) error {

	_, err := r.connection.Do("SET", id, req)
	return err
}

func (r Redis) GetString(shortUrl string) (string, error) {
	result, err := redis.String(r.connection.Do("GET", shortUrl))
	return result, err
}

func NewCache() Redis {
	var r Redis
	connection, _ := redis.Dial("tcp", "localhost:6379")
	r.connection = connection
	return r
}
