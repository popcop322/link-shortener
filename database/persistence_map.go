package database

import "fmt"

type PersistenceMap struct {
	m map[string]string
}

func (p PersistenceMap) SetLink(req string, id string) error {
	p.m[id] = req
	fmt.Println(p.m)
	return nil
}

func (p PersistenceMap) GetLink(shortUrl string) (string, error) {
	return p.m[shortUrl], nil
}

func NewPersistenceMap() PersistenceMap {
	var p PersistenceMap
	p.m = make(map[string]string)
	return p
}
