package database

import (
	"database/sql"
	"log"

	_ "github.com/lib/pq"
)

type IStorage interface {
	SetLink(req string, id string) error
	GetLink(shortUrl string) (string, error)
}

type Storage struct {
	database *sql.DB
}

func (s Storage) SetLink(req string, id string) error {

	_, err := s.database.Exec("insert into links (short_link, full_link) values ($1, $2)", id, req)
	return err
}

func (s Storage) GetLink(shortUrl string) (string, error) {
	row := s.database.QueryRow("select full_link from links where short_link = $1", shortUrl)
	var result string
	err := row.Scan(&result)
	return result, err
}

func NewStorage() Storage {
	var s Storage
	connStr := "user=postgres password=12345678 dbname=linkshortener sslmode=disable"
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		log.Fatal(err)
	}
	s.database = db
	return s
}
