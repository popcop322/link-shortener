module example

go 1.15

require (
	github.com/gomodule/redigo v1.8.3
	github.com/gorilla/mux v1.8.0
	github.com/lib/pq v1.9.0
	github.com/lithammer/shortuuid/v3 v3.0.5
)
