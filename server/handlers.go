package server

import (
	"encoding/json"
	"fmt"
	"net/http"

	"example/url"

	"github.com/lithammer/shortuuid/v3"
)

func (s Server) LongUrlHandler(w http.ResponseWriter, r *http.Request) {
	var req url.Request

	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	id := shortuuid.New()

	err = s.Cache.SetString(req.URL, id)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	err = s.Storage.SetLink(req.URL, id)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	err = json.NewEncoder(w).Encode(id)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	fmt.Println(id)
}

func (s Server) ShortUrlHandler(w http.ResponseWriter, r *http.Request) {
	shortUrl := GetIdFromUrl(r)

	result, err := s.Cache.GetString(shortUrl)
	if err != nil {
		fmt.Println("cache crashed or 404")
	}

	if result == "" {
		result, err = s.Storage.GetLink(shortUrl)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		err = s.Cache.SetString(result, shortUrl)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		result = result
	}

	http.Redirect(w, r, result, 302)
}
