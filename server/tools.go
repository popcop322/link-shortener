package server

import (
	"net/http"
	"strings"
)

func GetIdFromUrl(r *http.Request) (shortUrl string) {
	parts := strings.Split(r.URL.Path, "/")
	shortUrl = parts[2]
	return shortUrl
}
