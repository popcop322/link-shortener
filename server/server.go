package server

import (
	"example/cache"
	"example/database"
	"net/http"

	"github.com/gorilla/mux"
)

type Server struct {
	Cache   cache.Cache
	Storage database.IStorage
}

func (s Server) Start() {
	r := mux.NewRouter()
	r.HandleFunc("/url/", s.LongUrlHandler).Methods(http.MethodPost)
	r.HandleFunc("/url/{id}", s.ShortUrlHandler)

	server := http.Server{Addr: ":80", Handler: r}
	server.ListenAndServe()
}
