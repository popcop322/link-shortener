package main

import (
	"example/cache"
	"example/database"
	"example/server"
)

func main() {

	c := cache.NewCache()
	db := database.NewStorage()
	s := server.Server{Cache: c, Storage: db}
	s.Start()
}
